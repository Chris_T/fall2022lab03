//Christian Tran 1937391

package vehicles;

public class Bicycle{

    private String manufacturer;
    public String getManufacturer() {
        return manufacturer;
    }

    private int numberGears;
    public int getNumberGears() {
        return numberGears;
    }

    private double maxSpeed;
    public double getMaxSpeed() {
        return maxSpeed;
    }

    public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    public String toString() {
        return "Manufacturer: " + getManufacturer() + ", Number of Gears: " + getNumberGears() + ", MaxSpeed: " + getMaxSpeed();
    }
}